<?php
$page_title = 'Add Sale';
require_once('includes/load.php');
// Checkin What level user has permission to view this page
page_require_level(3);
?>
<pre><?php
$pdfData=$_POST['pdf'];

// $s=microtime(true);
pdfDirCreate();
file_put_contents('invoices/2021/filename.pdf', base64_decode($pdfData));
file_put_contents('/home/user/Documents/invoices/2021/filename.pdf', base64_decode($pdfData));




// if (isset($_POST['add_sale'])) {
  
//   // print_r($_POST['data']);
//   $data = json_decode($_POST['data'], true);
//   print_r($data);
//   $i = 1;
//   $length = count($data);

//   foreach ($data as $d) { 
//     $_POST['s_id'] = $d['id'];
//     $_POST['quantity'] = $d['qty'];
//     $_POST['total'] = $d['total'];
//     $_POST['price'] = $d['price'];
//     $_POST['cname'] = $d['cname'];
//     $_POST['billNumber'] = $d['billNumber'];

//     $req_fields = array('s_id', 'quantity', 'price', 'total', 'cname');
//     validate_fields($req_fields);
//     if (empty($errors)) {
//       $p_id      = $db->escape((int)$_POST['s_id']);
//       $s_qty     = $db->escape((int)$_POST['quantity']);
//       $s_total   = $db->escape($_POST['total']);
//       // $date      = $db->escape($_POST['date']);
//       $date      = make_date();
//       $s_date    = make_date();
//       $cname = $_POST['cname'];
//       $billNumber = $_POST['billNumber'];

//       $sql  = "INSERT INTO sales (";
//       $sql .= " product_id,qty,price,date,customerName,billNumberID";
//       $sql .= ") VALUES (";
//       $sql .= "'{$p_id}','{$s_qty}','{$s_total}','{$s_date}','{$cname}','{$billNumber}'";
//       $sql .= ")";

//       if ($db->query($sql)) {
//         update_product_qty($s_qty, $p_id);
//           $session->msg('s', "Sale added. ");
//         if ($i == $length) {
//           redirect('add_sale.php', false);
//         }
//         $i++;
//       } else {
//         $session->msg('d', ' Sorry failed to add!');
//         redirect('add_sale.php', false);
//       }
//     } else {
//       $session->msg("d", $errors);
//       redirect('add_sale.php', false);
//     }
//   }
// }
?></pre>
<?php include_once('layouts/header.php'); ?>
<div class="row">
  <div class="col-md-6">
    <?php echo display_msg($msg); ?>
    <form method="post" action="ajax.php" autocomplete="off" id="sug-form">
      <div class="form-group">
        <div class="input-group">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-primary">Find It</button>
          </span>
          <input type="text" id="sug_input" class="form-control" name="title" placeholder="Search for product name">
        </div>
        <div id="result" class="list-group"></div>
      </div>
    </form>
  </div>
</div>
<div class="row">

  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          <span>Sale Edit</span>
        </strong>
      </div>
      <div class="panel-body">
        <!-- <form method="post" action="add_sale.php"> -->
        <table class="table table-bordered" border="1">
          <thead>
            <th> Item </th>
            <th> Price </th>
            <th> Qty </th>
            <th> Total </th>
            <th> Date</th>
            <th> Action</th>
          </thead>
          <tbody id="product_info"> </tbody>
        </table>
        <!-- </form> -->
      </div>
    </div>
  </div>


  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          <span>Customer Form</span>
        </strong>
      </div>
      <div class="panel-body">
        <form>
          <label for="customername"> Customer's Name </label><br>
          <input type="text" id="setCustomerName">
          <br>
          <br>

         <label for="caddresss">Customer's Address </label ><br>
         <input type="text" id="setCustomerAddress">
          
          <br>
          <br>
        <label for="cpan"> Buyer's Pan Number </label ><br>
        <input type="text" id="setCustomerPan">
          <br>
          <br>

          Mode of Payment :
          <select id="modeOfPayment">
            <option value="Cheque">Cheque</option>
            <option value="Cash">Cash</option>
            <option value="Credit">Credit</option>
          </select>
          <br>
          <br>
          <button type="button" onclick="setData()" class="ml-5 btn btn-primary float-end">Set Data</button>
        </form>
      </div>
    </div>
  </div>

  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          <span>Bills Generator</span>
          <span style="float:right">
            <button onclick="savedata(true)" class="btn btn-primary " id="ps"> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-printer" viewBox="0 0 16 16">
                <path d="M2.5 8a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z" />
                <path d="M5 1a2 2 0 0 0-2 2v2H2a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h1v1a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-1h1a2 2 0 0 0 2-2V7a2 2 0 0 0-2-2h-1V3a2 2 0 0 0-2-2H5zM4 3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1v2H4V3zm1 5a2 2 0 0 0-2 2v1H2a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-1v-1a2 2 0 0 0-2-2H5zm7 2v3a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1v-3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1z" />
              </svg> </button>

          </span>
        </strong>
      </div>
      <div class="panel-body" id="bill">
        <p>Reg No:- 20124/45</p>
        <div>
        </div>
        <p>Vat No:-123456789</p>
        <p style="display: inline;">Bill Number:-</p> 
        <p style="display: inline;" id="billNumber">1</p>

        <h3 class="text-center">Vendor Name</h1>
          <h6 class="text-center">Phone No:-012345789, Address:-lalipur</h1>
            <br>
            <br>
            <div class="d-flex justify-content-between"></div>
            <p style="display: inline;">Customer Name:- </p><p style="display: inline;" id="clientname"></p>
            <br><p style="display: inline;"></p>Customer Address:- <p style="display: inline;" id="clientAdd"></p>
            <br><p style="display: inline;"></p>Customer Pan no:- <p style="display: inline;" id="clientPan"></p>
            <br><p style="display: inline;"></p>Payment Mode:- <p style="display: inline;" id="clientPayMode"></p>
            <br>

            <br>
            <table class="table table-bordered" id="mytable" border="1">
              <thead>
                <th> Item </th>
                <th> Price </th>
                <th> Qty </th>
                <th> Total </th>
                <th> Action</th>
              </thead>
              <tbody id="listitems"> </tbody>
              <tfoot>
                <tr>
                  <td colspan="3">
                    <p class="text-center">Vat Amount</p>
                  </td>
                  <td id="vatAmount">0</td>

                </tr>
                <tr>
                  <td colspan="3">
                    <p class="text-center">Grand Total with VAT</p>
                  </td>
                  <td id="gtotal">0</td>

                </tr>
              </tfoot>
            </table>
      </div>
      <form method="post" action="add_sale.php" id="form">

        <input type="hidden" name="data" id="data">
        <input type="hidden" name="pdf" id="pdf">
        <button type="submit" onclick="savedata(false)" class="btn btn-primary " id="s" name="add_sale"> Save</button>
      </form>
    </div>
  </div>
  <button onclick="generatePDF()">generatePDF</button>
</div>
<script src="libs\js\jquery.min.js"></script>
<script src="libs\js\pdf.js"></script>
<script src="libs\js\Print.js"></script>
<script>
  function setData() {
    $('#clientname').html( $('#setCustomerName').val())
    $('#clientAdd').html( $('#setCustomerAddress').val())
    $('#clientPan').html( $('#setCustomerPan').val())
    $('#clientPayMode').html($('#modeOfPayment').val())

    // document.getElementById('clientname').innerHTML=clientname;
  }
  clicked = 0

  function savedata(flag) {
    if (flag == true) {
      if (clicked < 1) {
        $("#mytable th:last-child, #listitems td:last-child").remove();
        $('#ps').hide
        $('#s').hide
        clicked++;
      }
      printData();
    }
    generatePDF();
    listOfitems = [];
    table = document.getElementById('mytable');

    var lengthOfTable = table.rows.length;
    console.log('the length' + lengthOfTable);
    for (i = 1; i < lengthOfTable - 2; i++) {
      console.log('this is on i loop')
      x = document.getElementById('mytable').rows[i].cells;
      listOfitems.push({
        name: x[0].childNodes[0].nodeValue,
        price: x[1].innerHTML,
        qty: x[2].innerHTML,
        total: x[3].innerHTML,
        id: x[0].childNodes[1].value,
        cname: document.getElementById('clientname').innerHTML,
        billNumber: document.getElementById('billNumber').innerHTML
      })
    }
    console.log(listOfitems);
    a = JSON.stringify(listOfitems);
    document.getElementById('data').value = a;

    // console.log(document.getElementById('data'));
    document.getElementById('form').submit;

  }

  function printData() {
    // jquery plugin that print with css
    printJS({
      printable: 'bill',
      type: 'html',
      targetStyles: ['*']
    })
    //for the normal print view without any css or bootstrap theme
    // var divToPrint = document.getElementById("bill");
    // newWin = window.open("");
    // newWin.document.write(divToPrint.outerHTML);
    // newWin.print();
    // newWin.close();
  }

  function addrow() {
    price = $('input[name=price]').val()
    id = $('input[name=s_id]').val()
    quantity = $('input[name=quantity]').val()
    totals = $('input[name=total]').val()
    date = $('input[name=date]').val()
    nam = $('#s_name').html()

    // console.log(html);
    $('#listitems').append("<tr><td>" + nam + "<input type=\"hidden\"  value=\"" + id + "\"></td><td>" + price +
      "</td><td>" + quantity + "</td><td>" + totals +
      "</td><td><input type='button' value='Delete' onclick= 'removeRow(this)'></td></tr>");
    grandtotal();
  }

  function removeRow(row) {
    $(row).parent().parent().remove();
    console.log("deleting");
    grandtotal();
  }

  function grandtotal() {
    console.log("called");

    var table = $("#mytable tbody");
    var gtotal = 0;

    table.find('tr').each(function(i, el) {
      var tds = $(this).find("td:eq(3)").html();
      //  v= $tds.eq(3). $("input['name]").val();
      console.log(tds);

      gtotal += +tds;
      console.log("the grnd:" + gtotal);
      // do something with productId, product, Quantity
    });
    console.log("the grand total" + gtotal);
    var vatamount = (13 / 100) * gtotal;;
    console.log(vatamount);
    $('#vatAmount').html(vatamount);

    $('#gtotal').html(gtotal + vatamount);

  }

  function generatePDF() {
    // Choose the element that our invoice is rendered in.
    const element = document.getElementById('bill');
    // Choose the element and save the PDF for our user.
    html2pdf().from(element).outputPdf().then(function(pdf) {
    //Convert to base 64
            const newpdf=btoa(pdf);
            console.log(newpdf)
    document.getElementById('pdf').value = newpdf;

        // })
    });
    
  }
</script>
<?php include_once('layouts/footer.php'); ?>